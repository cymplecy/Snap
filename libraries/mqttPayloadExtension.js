
/*  Sources of inspiration
https://forum.snap.berkeley.edu/t/base64-encoding-decoding/10635
https://forum.snap.berkeley.edu/t/decode-base64-string/11294
https://forum.snap.berkeley.edu/t/async-js-in-snap/1269
https://json5.org
*/

SnapExtensions.primitives.set(
    'pay_b64_costume2b64img(costume)',
    function (costume) {
		try {
			return costume.contents.toDataURL();
		} catch (e) {
			return '';
		}
    }
);

SnapExtensions.primitives.set(
    'pay_b64_b64img2costume(b64)',
    function (b64) {
		let res = '';
		if (!b64) {
			window._b64_b64img2costume_resp = '';
			window._b64_b64img2costume_pend = false;
			return res;
		}

		if (!window._b64_b64img2costume_pend) {
			window._b64_b64img2costume_resp = '';
			window._b64_b64img2costume_pend = true;
			let resp;
			let img = new Image();
			let canvas = document.createElement('canvas');
			img.src = b64;
			
			img.onload = function() {
				canvas.width = img.width;
				canvas.height = img.height;
				let context = canvas.getContext('2d');
				context.drawImage(img, 0, 0, img.width, img.height);
				resp = new Costume(canvas);
				canvas.remove();
				window._b64_b64img2costume_resp = resp;
			}
		}

		if (window._b64_b64img2costume_resp) {
			res = window._b64_b64img2costume_resp;
			window._b64_b64img2costume_resp = '';
			window._b64_b64img2costume_pend = false;
		}
		
		return res;
	}
);

SnapExtensions.primitives.set(
    'pay_js5_get(json,attrOrPos)',
    function (json,attrOrPos) {
		let res = '';
		try {
		  let jsp = JSON5.parse(json);
		  if (attrOrPos.startsWith('[')) {
			res = eval('jsp' + attrOrPos);
		  } else {
			res = eval('jsp.' + attrOrPos);
		  }
		  res = res ? res : '';
		  if (typeof res == 'string') {
			return res;
		  } else {
			return JSON5.stringify(res,null,2);
		  }
		} catch(e) {
		  return '';
		}
    }
);

SnapExtensions.primitives.set(
    'pay_js5_set(json,attrOrPos,value)',
    function (json,attrOrPos,value) {
		let res = '';
		let jsp = '';
		try {
		  jsp = JSON5.parse(json);
		  if (attrOrPos.startsWith('[')) {
			try {
			  eval('jsp' + attrOrPos + ' = ' + value);
			} catch(e1) {
			  eval('jsp' + attrOrPos + ' = "' + value + '"');
			}
		  } else {
			try {
			  eval('jsp.' + attrOrPos + ' = ' + value);
			} catch(e2) {
			  eval('jsp.' + attrOrPos + ' = "' + value + '"');
			}
		  }
		} catch(e3) {}
		if (jsp != '') {
			res = JSON5.stringify(jsp, null, 2);
		}
		return res;    
	}
);

SnapExtensions.primitives.set(
    'pay_js5_length(json)',
    function (json) {
		let res = 1;
		try {
		  jsp = JSON5.parse(json);
		  res = jsp.length;
		} catch(e) {
		}
		return res ? res : 1;
    }
);

